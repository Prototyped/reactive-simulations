package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import scala.annotation.tailrec

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  //
  // to complete with tests for orGate, demux, ...
  //

  test("orGate truth table") {
    testOrGate(orGate)
  }

  test("orGate2 truth table") {
    testOrGate(orGate2)
  }

  def testOrGate(gateFun: (Wire, Wire, Wire) => Unit) {
    val a, b, out = new Wire
    gateFun(a, b, out)
    a.setSignal(false)
    b.setSignal(false)
    run

    assert(out.getSignal === false, "false OR false = false")

    a.setSignal(true)
    run

    assert(out.getSignal === true, "true OR false = true")

    b.setSignal(true)
    run

    assert(out.getSignal === true, "true OR true = true")

    a.setSignal(false)
    run

    assert(out.getSignal === true, "false OR true = true")
  }

  def testDemux(inVal: Boolean, n: Int, pos: Int) {
    val inWire = new Wire
    val controlWires = List.fill(n)(new Wire)
    val outWires = List.fill(1 << n)(new Wire)
    demux(inWire, controlWires, outWires)

    inWire.setSignal(inVal)
    @tailrec
    def setBits(pos: Int, index: Int, controlWires: List[Wire]) {
      controlWires(index).setSignal(((pos >> (controlWires.size - index - 1)) & 1) == 1)
      if (index <= 0) ()
      else setBits(pos, index - 1, controlWires)
    }
    if (controlWires.isEmpty) ()
    else setBits(pos, controlWires.size - 1, controlWires)

    run

    @tailrec
    def verifyOutputWires(inVal: Boolean, pos: Int, index: Int, outWires: List[Wire]) {
      assert(outWires(index).getSignal === (inVal && outWires.size - index - 1 == pos))
      if (index <= 0) ()
      else verifyOutputWires(inVal, pos, index - 1, outWires)
    }
    verifyOutputWires(inVal, pos, outWires.size - 1, outWires)
  }
  test("demux") {
    testDemux(true, 0, 0)
    testDemux(false, 0, 0)
    testDemux(true, 1, 0)
    testDemux(false, 1, 0)
    testDemux(true, 1, 1)
    testDemux(false, 1, 1)
    testDemux(true, 4, 3)
    testDemux(false, 4, 3)
  }
}
