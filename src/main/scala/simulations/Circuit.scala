package simulations

import common._

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal
  
  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }
}

abstract class CircuitSimulator extends Simulator {

  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

  //
  // to complete with orGates and demux...
  //

  def orGate(a1: Wire, a2: Wire, output: Wire) {
    def orAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(OrGateDelay) { output.setSignal(a1Sig | a2Sig) }
    }
    a1 addAction orAction
    a2 addAction orAction
  }
  
  def orGate2(a1: Wire, a2: Wire, output: Wire) {
    val notA1Wire, notA2Wire, notA1AndNotA2Wire = new Wire
    inverter(a1, notA1Wire)
    inverter(a2, notA2Wire)
    andGate(notA1Wire, notA2Wire, notA1AndNotA2Wire)
    inverter(notA1AndNotA2Wire, output)
  }

  def demux(in: Wire, c: List[Wire], out: List[Wire]) {
    if (c.isEmpty) {
      val midWire = new Wire
      inverter(in, midWire)
      inverter(midWire, out(0))
    } else {
      val notHeadWire, highWire, lowWire, recursiveOutWire = new Wire
      inverter(c.head, notHeadWire)
      andGate(c.head, in, highWire)
      andGate(notHeadWire, in, lowWire)
      demux(highWire, c.tail, out.take(1 << c.tail.size))
      demux(lowWire, c.tail, out.drop(1 << c.tail.size))
    }
  }

}

object Circuit extends CircuitSimulator {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5

  def andGateExample() {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  //
  // to complete with orGateExample and demuxExample...
  //

  def orGateExample() {
    val in1, in2, out = new Wire
    orGate2(in1, in2, out)
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  def demuxExample() {
    val in = new Wire
    val control = List.fill(3)(new Wire)
    val out = List.fill(1 << 3)(new Wire)
    demux(in, control, out)
    probe("in", in)
    for (i <- 0 until control.size) { probe(s"control_$i", control(i)) }
    for (i <- 0 until out.size) { probe(s"out_$i", out(i)) }
    in.setSignal(true)
    run

    control(0).setSignal(true)
    run

    control(2).setSignal(true)
    run

    control(0).setSignal(false)
    run

    in.setSignal(false)
    run
  }
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
  println("AND gate example:")
  Circuit.andGateExample()
  println("OR gate example:")
  Circuit.orGateExample()
  println("Demux example:")
  Circuit.demuxExample()
}
