package simulations

import math.random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8

    // to complete: additional parameters of simulation
    val transmissibility: Double = 0.4
    val prevalence: Double = 0.01
    val minMoveDays: Int = 1
    val maxMoveDays: Int = 5
    val infectedToSickDays: Int = 6
    val infectedToDeathDays: Int = 14
    val mortality = 0.25
    val infectedToImmuneDays = 16
    val infectedToHealthyDays = 18
    val flight = 0.01
    val mobility = 0.5
    val sickMobility = 0.5 * mobility
    val chosenFewVaccine = 0.0 // 0.05
  }

  import SimConfig._

  var personId = 0
  val persons: List[Person] = List.fill(population)(new Person({personId = personId + 1; personId}))

  class Person (val id: Int) {
    val chosenFew = randomBelow((1.0 / chosenFewVaccine).toInt) == 0
    var infected = !chosenFew && id % (1.0 / prevalence).toInt == 0
    var sick = false
    var immune = false
    var dead = false

    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    //
    // to complete with simulation logic
    //

    sealed abstract class MoveDirection {
      def newPosition(row: Int, col: Int): (Int, Int)
    }

    case class Up() extends MoveDirection {
      override def newPosition(row: Int, col: Int) = ((row + roomRows - 1) % roomRows, col)
    }

    case class Down() extends MoveDirection {
      override def newPosition(row: Int, col: Int) = ((row + 1) % roomRows, col)
    }

    case class Left() extends MoveDirection {
      override def newPosition(row: Int, col: Int) = (row, (col + roomColumns - 1) % roomColumns)
    }

    case class Right() extends MoveDirection {
      override def newPosition(row: Int, col: Int) = (row, (col + 1) % roomColumns)
    }

    val possibleDirections = List(Up(), Down(), Left(), Right())

    def roomHasSickPeople(row: Int, col: Int): Boolean = persons.exists(p => p.row == row && p.col == col && p.sick)
    def newRandomRoom: Option[(Int, Int)] = Some((randomBelow(roomRows), randomBelow(roomColumns)))
    def newNeighboringRoom: Option[(Int, Int)] = {
      val viableRooms = for (d <- possibleDirections;
                             (r, c) = d.newPosition(row, col)
                             if !roomHasSickPeople(r, c))
        yield (r, c)
      if (viableRooms.isEmpty) None
      else {
        val pickedRoom = randomBelow(viableRooms.size)
        Some(viableRooms(pickedRoom))
      }
    }
    def newRoomWithAirChance: Option[(Int, Int)] =
      if (airChance) newRandomRoom
      else newNeighboringRoom
    def newRestrictedMoveRoom: Option[(Int, Int)] = if (mobilityChance) newNeighboringRoom else None

    def nextMoveTime: Int = minMoveDays + randomBelow(maxMoveDays - minMoveDays)

    def infectionChance: Boolean = randomBelow((1.0 / transmissibility).toInt) == 0
    def othersInfected: Boolean = persons.exists(p => p != this && p.row == row && p.col == col && (p.infected || p.immune))
    def isNowInfected: Boolean = infected || (!chosenFew && infectionChance && othersInfected)

    def airChance: Boolean = randomBelow((1.0 / flight).toInt) == 0
    def mobilityChance: Boolean = randomBelow((1.0 / (if (sick) sickMobility else mobility)).toInt) == 0

    def actionOnNewInfection() {
      afterDelay(infectedToSickDays) {
        sick = true
      }

      afterDelay(infectedToDeathDays) {
        dead = randomBelow((1.0 / mortality).toInt) == 1
        sick ||= dead
        immune &&= !dead
      }

      afterDelay(infectedToImmuneDays) {
        if (!dead) {
          sick = false
          immune = true
        }
      }

      afterDelay(infectedToHealthyDays) {
        if (immune) {
          infected = false
          sick = false
          immune = false
        }
      }
    }

    def moveAction(roomPicker: => Option[(Int, Int)]) {
      afterDelay(nextMoveTime) {
        if (!dead) {
          for ((r, c) <- roomPicker) {
            row = r
            col = c

            val newInfectionStatus = isNowInfected
            if (!infected && newInfectionStatus) {
              // state transition from healthy to infected
              infected = true
              actionOnNewInfection()
            }
          }
          moveAction(roomPicker)
        }
      }
    }

    if (infected)
      actionOnNewInfection()

    moveAction(newNeighboringRoom)
  }
}
